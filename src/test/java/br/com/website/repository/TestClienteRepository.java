package br.com.website.repository;
import br.com.website.model.Cliente;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class TestClienteRepository {

	/*@Autowired instância a classe automaticamente*/
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Test
	public void testSalvar(){
		
		Cliente cli = new Cliente("João","joão@gmail.com");
		Cliente cliSalvo = clienteRepository.save(cli);
		org.junit.Assert.assertNotNull(cliSalvo.getId());
	}
}
