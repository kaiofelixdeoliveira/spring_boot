package br.com.website.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Columns;

//entidade no banco de dados
@Entity
public class Cliente {

	//chave primaria  //auto-incremento
	@Id               @GeneratedValue 
	private long id;
	
	private String nome;
	
	@Column(nullable=false,length=50)
	private String email;

	public Cliente() {
		// TODO Auto-generated constructor stub
	}
	public Cliente(String nome,String email) {
		this.nome=nome;
		this.email=email;
		// TODO Auto-generated constructor stub
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
}
