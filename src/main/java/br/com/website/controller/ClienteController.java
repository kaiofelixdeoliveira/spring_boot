package br.com.website.controller;


import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.website.model.Cliente;
import br.com.website.repository.ClienteRepository;

@Named
@ViewScoped
public class ClienteController {
	
	@Autowired
	ClienteRepository clienteRepository;
	Cliente cliente = new Cliente();

	

	public void salvar(){
		clienteRepository.save(getCliente());
		
	}
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
