package br.com.website;
//ARQUIVO DE CONFIGURAÇÃO PRINCIPAL DO SPRING
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication                //extender para conseguir acessar recursos do tipo web
public class ProjetoSpringApplication extends SpringBootServletInitializer {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		return application.sources(ProjetoSpringApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(ProjetoSpringApplication.class, args);
	}
}
