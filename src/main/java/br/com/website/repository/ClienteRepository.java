package br.com.website.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.cdi.JpaRepositoryExtension;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;

import br.com.website.model.Cliente;

//Equivalente a classe DAO
//ao invés de class será interface
public interface ClienteRepository extends JpaRepository<Cliente,Long>{

	
}
